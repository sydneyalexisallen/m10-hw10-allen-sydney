"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

// arrow function start
var boil;

boil = function boil() {
    return "212";
};

document.getElementById("boiling-temp").innerHTML = boil();

var freeze;

freeze = function freeze() {
    return "32";
};

document.getElementById("freezing-temp").innerHTML = freeze();

var water;
water = function water() {
    return "75";
};

document.getElementById("water-temp").innerHTML = water();
//arrow funtion stop


console.log(_typeof(parseInt(water())));

if (parseInt(boil()) > 212) {
    setTimeout(function () {
        document.getElementById("boiling").classList.remove("hide");
    }, 3000);
}

if (parseInt(boil()) <= 212) {
    setTimeout(function () {
        document.getElementById("boiling").classList.add("hide");
    }, 3000);
    console.log("it's not hot");
}

if (parseInt(freeze()) < 32) {
    setTimeout(function () {
        document.getElementById("frozen").classList.remove("hide");
    }, 3000);

    console.log("it's cold");
}

if (parseInt(freeze()) >= 32) {
    setTimeout(function () {
        document.getElementById("frozen").classList.add("hide");
    }, 3000);
    console.log("it's not cold");
}

if (parseInt(water()) > 32 && parseInt(water()) < 212) {
    setTimeout(function () {
        document.getElementById("good-temp").classList.remove("hide");
    }, 3000);

    console.log("the water is fine");
}

if (parseInt(water()) > 212 && parseInt(water()) < 32) {
    setTimeout(function () {
        document.getElementById("good-temp").classList.add("hide");
    }, 3000);

    console.log("the water is not fine");
}

//setTimeout start 


//let x = getTemp(212, 1);
//function getTemp (a,b){
//   return a + b;
//}


//if (boil > 212) {
//  setTimeout(function(){ document.getElementById("boiling").classList.add("show"); }, 3000);
//} else{
//  setTimeout(function(){ document.getElementById("boiling").classList.remove("show"); }, 3000);
//}


//setTimeout stop